const options = {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
};
export const formatDate = date => {
  return new Date(date).toLocaleDateString('en-US', options);
};
