/* eslint-disable object-shorthand */
/* eslint-disable func-names */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const CheckIns = new Mongo.Collection('CheckIns');

Meteor.methods({
  'checks.checkIn'(personId) {
    if (!CheckIns.find({ personId: personId }).count()) {
      CheckIns.insert({
        checkInAt: new Date(),
        personId: personId,
      });
    }
    return true;
  },
});
