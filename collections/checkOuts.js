/* eslint-disable object-shorthand */
/* eslint-disable func-names */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const CheckOuts = new Mongo.Collection('CheckOuts');

Meteor.methods({
  'checks.checkOut'(personId) {
    if (!CheckOuts.find({ personId }).count()) {
      CheckOuts.insert({
        checkOutAt: new Date(),
        personId: personId,
      });
    }
  },
});
