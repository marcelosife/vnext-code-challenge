import { Mongo } from 'meteor/mongo';

export const PeopleGrouped = new Mongo.Collection('PeopleGrouped');

export const PeopleNotCheckIn = new Mongo.Collection('PeopleNotCheckIn');
