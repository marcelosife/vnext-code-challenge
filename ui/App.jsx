import React from 'react';
import { TEXTS } from '../shared/constants';
import Home from './Home';
import Container from '@material-ui/core/Container';
import { Meteor } from 'meteor/meteor';
import Typography from '@material-ui/core/Typography';

export const App = () => {
  const communitiesHandler = Meteor.subscribe('communities.public');

  return (
    <>
      <Container maxWidth="sm">
        <Typography variant="h2" component="h1">
          {TEXTS.HOME_TITLE}
        </Typography>
      </Container>

      <Home communitiesHandler={communitiesHandler} />
    </>
  );
};
