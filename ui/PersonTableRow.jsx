import React from 'react';
import { Meteor } from 'meteor/meteor';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Fab from '@material-ui/core/Fab';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import { formatDate } from '../shared/utils';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';

const isShowCheckOutButton = checkInTime =>
  (new Date() - new Date(checkInTime)) / 1000 < 5;

const onClickCheckIn = (personID, setLoading) => {
  setLoading(true);
  Meteor.call('checks.checkIn', personID, () => {
    setLoading(false);
  });
};

const onClickCheckOut = (personId, setLoadingcheckOut) => {
  setLoadingcheckOut(true);
  Meteor.call('checks.checkOut', personId, () => {
    setLoadingcheckOut(false);
  });
};

export const PersonTableRow = ({ checkIn, checkOut, person }) => {
  const [isLoadingCheckIn, setLoadingcheckIn] = React.useState(false);
  const [isLoadingCheckOut, setLoadingcheckOut] = React.useState(false);

  return (
    <TableRow>
      <TableCell align="center">
        <Fab
          color="primary"
          size="small"
          onClick={() => onClickCheckIn(person._id, setLoadingcheckIn)}
        >
          <CheckIcon />
        </Fab>
      </TableCell>
      <TableCell align="center">
        {checkIn &&
        checkIn.checkInAt &&
        isShowCheckOutButton(checkIn.checkInAt) ? (
          <Fab
            color="secondary"
            size="small"
            onClick={() => onClickCheckOut(person._id, setLoadingcheckOut)}
          >
            <CloseIcon />
          </Fab>
        ) : (
          ' '
        )}
      </TableCell>
      <TableCell align="left">
        {person.firstName} {person.lastName}
      </TableCell>
      <TableCell align="left">{person.companyName}</TableCell>
      <TableCell align="left">{person.title}</TableCell>
      <TableCell align="center">
        {checkIn && checkIn.checkInAt ? formatDate(checkIn.checkInAt) : ''}
        {isLoadingCheckIn && <HourglassEmptyIcon />}
      </TableCell>
      <TableCell align="center">
        {checkOut && checkOut.checkOutAt ? formatDate(checkOut.checkOutAt) : ''}
        {isLoadingCheckOut && <HourglassEmptyIcon />}
      </TableCell>
    </TableRow>
  );
};
