import React, { useEffect } from 'react';
import Container from '@material-ui/core/Container';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Communities } from '../collections/communities';
import { withTracker } from 'meteor/react-meteor-data';
import PeopleList from './PeopleList';
import Paper from '@material-ui/core/Paper';

const renderItems = communities => {
  return communities.map(community => (
    <MenuItem key={community._id} value={community._id}>
      {community.name}
    </MenuItem>
  ));
};

function Home({ communities, communitiesHandler }) {
  const [selectedCommunity, setCommunity] = React.useState(0);
  // on select an event subscribe again
  const onSelectEvent = event => {
    Meteor.subscribe('people.checks', event.target.value);
    Meteor.subscribe('peopleEventTotals', event.target.value);
    Meteor.subscribe('peopleNotCheckIn', event.target.value);
    setCommunity(event.target.value);
  };

  // if the communities done loading subscribes
  useEffect(() => {
    if (communitiesHandler.ready()) {
      Meteor.subscribe('people.checks', communities[0]._id);
      Meteor.subscribe('peopleEventTotals', communities[0]._id);
      Meteor.subscribe('peopleNotCheckIn', communities[0]._id);
      setCommunity(communities[0]._id);
    }
  }, [communities, communitiesHandler]);

  return (
    <Paper>
      <Container maxWidth="sm" className="app-container">
        <form autoComplete="off">
          <FormControl className="form-control">
            <InputLabel shrink>Events</InputLabel>
            <Select value={selectedCommunity} onChange={onSelectEvent}>
              <MenuItem value={0}>Select</MenuItem>
              {renderItems(communities)}
            </Select>
          </FormControl>
        </form>
      </Container>
      {selectedCommunity ? (
        <PeopleList communityId={selectedCommunity} />
      ) : null}
    </Paper>
  );
}

export default withTracker(() => {
  return {
    communities: Communities.find().fetch(),
  };
})(Home);
