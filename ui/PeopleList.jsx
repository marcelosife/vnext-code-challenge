import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { People } from '../collections/people';
import { withTracker } from 'meteor/react-meteor-data';
import { PersonTableRow } from './PersonTableRow';
import { PeopleGrouped } from '../collections/peopleGrouped';
import { PeopleNotCheckIn } from '../collections/peopleGrouped';
import PeopleHeaderCounters from './PeopleHeaderCounters';

const PeopleList = ({
  communityId,
  people,
  peopleGroupedByCompany,
  peopleNotCheckIn,
}) => {
  const filteredPeople = people.filter(
    person => person.communityId === communityId
  );

  return (
    <Paper className="app-container">
      <PeopleHeaderCounters
        peopleNotCheckIn={peopleNotCheckIn}
        peopleGroupedByCompany={peopleGroupedByCompany.filter(
          peopleGrouped => peopleGrouped._id !== null
        )}
      />
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="center">Check-in</TableCell>
            <TableCell align="center">Check-out</TableCell>
            <TableCell align="left">Full Name</TableCell>
            <TableCell align="left" style={{ width: 100 }}>
              Company Name
            </TableCell>
            <TableCell align="left" style={{ width: 150 }}>
              Title
            </TableCell>
            <TableCell align="center">Check-in Date </TableCell>
            <TableCell align="center">Check-out Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {filteredPeople.map(person => (
            <PersonTableRow
              key={person._id}
              checkIn={person.checkIns[0]}
              checkOut={person.checkOuts[0]}
              person={person}
            />
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default withTracker(({ communityId }) => {
  return {
    people: People.find({ communityId }).fetch(),
    peopleGroupedByCompany: PeopleGrouped.find({ communityId }).fetch(),
    peopleNotCheckIn: PeopleNotCheckIn.find({ communityId }).fetch(),
  };
})(PeopleList);
