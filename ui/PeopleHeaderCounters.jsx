import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Paper } from '@material-ui/core';

const PeopleHeaderCounters = ({ peopleNotCheckIn, peopleGroupedByCompany }) => {
  let peopleInEvent = 0;
  // sum the people in the peopleGroupedByCompany list count
  if (peopleGroupedByCompany.length) {
    peopleGroupedByCompany.map(peopleGrouped => {
      peopleInEvent += peopleGrouped.count;
    });
  }
  return (
    <>
      <Paper className="people-count">
        <Typography variant="h6">
          People not check-in (
          {peopleNotCheckIn.length ? peopleNotCheckIn.length : 0}) • People in
          the Event ({peopleInEvent})
        </Typography>
      </Paper>
      <Paper className="people-count">
        <Typography variant="h6">People in the Event by Company:</Typography>
        {peopleGroupedByCompany.map(peopleGrouped => (
          <span variant="subtitle1" key={peopleGrouped._id}>
            {peopleGrouped._id}({peopleGrouped.count}) •{' '}
          </span>
        ))}
      </Paper>
    </>
  );
};
export default PeopleHeaderCounters;
