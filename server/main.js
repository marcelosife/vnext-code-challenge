/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */
import { Meteor } from 'meteor/meteor';
import { loadInitialData } from './initial-data';
import { Communities } from '../collections/communities';
import { People } from '../collections/people';
import { CheckIns } from '../collections/checkIns';
import { CheckOuts } from '../collections/checkOuts';
import { ReactiveAggregate } from 'meteor/jcbernack:reactive-aggregate';

Meteor.startup(() => {
  loadInitialData();
  //get all communities
  Meteor.publish('communities.public', function() {
    return Communities.find({
      fields: Communities.publicFields,
    });
  });

  // get all people, join with check-in and check-out filtering by the community
  Meteor.publish('people.checks', function(communityId) {
    ReactiveAggregate(this, People, [
      {
        $lookup: {
          from: CheckIns,
          localField: '_id',
          foreignField: 'personId',
          as: 'checkIns',
        },
      },
      {
        $lookup: {
          from: CheckOuts,
          localField: '_id',
          foreignField: 'personId',
          as: 'checkOuts',
        },
      },
      { $match: { communityId } },
    ]);
  });

  // count people grouped by company filtering by community
  // that check-in and didnt ckeck-out
  Meteor.publish('peopleEventTotals', function(communityId) {
    ReactiveAggregate(
      this,
      People,
      [
        { $match: { communityId } },
        {
          // join check-in
          $lookup: {
            from: CheckIns,
            localField: '_id',
            foreignField: 'personId',
            as: 'checkIns',
          },
        },
        {
          // joing check-out
          $lookup: {
            from: CheckOuts,
            localField: '_id',
            foreignField: 'personId',
            as: 'checkOuts',
          },
        },
        { $unwind: '$checkIns' },
        // filter of people that didnt checkout
        { $match: { checkOuts: { $size: 0 } } },
        {
          $group: {
            _id: '$companyName',
            communityId: { $push: '$communityId' },
            count: { $sum: 1 },
          },
        },
        { $unwind: '$communityId' },
      ],
      { clientCollection: 'PeopleGrouped' }
    );
  });
  // get the people that didnt check-in
  Meteor.publish('peopleNotCheckIn', function(communityId) {
    ReactiveAggregate(
      this,
      People,
      [
        { $match: { communityId } },
        {
          $lookup: {
            from: CheckIns,
            localField: '_id',
            foreignField: 'personId',
            as: 'checkIns',
          },
        },
        { $match: { checkIns: { $size: 0 } } },
      ],
      { clientCollection: 'PeopleNotCheckIn' }
    );
  });
});
